# Path to ohmyzsh installation
export ZSH="$HOME/.oh-my-zsh"

# Set ohmyzsh theme: https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="fwalch"

# Enable ohmyzsh command auto-correction
ENABLE_CORRECTION="false"

# Enable auto updates
DISABLE_UPDATE_PROMPT=true

# Plugins for ohmyzsh: https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins
plugins=(
  themes
  git
  fnm
  # git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
  zsh-autosuggestions
)

# Source ohmyzsh
source $ZSH/oh-my-zsh.sh

# Set vscode as editor
export VISUAL=code
export EDITOR="$VISUAL"

# Homebrew setup
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Add bin to PATH
export PATH="$HOME/.local/bin:$PATH"

# Setup fnm
FNM_PATH="/home/flippidippi/.local/share/fnm"
if [ -d "$FNM_PATH" ]; then
  export PATH="/home/flippidippi/.local/share/fnm:$PATH"
  eval "`fnm env`"
fi

# WSL specific commands
if type wslpath > /dev/null; then
  # Windows IP
  export WINDOWS_IP=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}')

  # Default browser
  export BROWSER=/usr/bin/wslview
fi

# Aliases
alias edit="code"
alias ...=".. && -"
alias programming="~/Documents/Programming"
alias work="~/Documents/Work"
alias reload="omz reload"
alias gbp="git branch -vv | grep ': gone]'|  grep -v '*' | awk '{ print \$1; }' | xargs -r git branch -D"